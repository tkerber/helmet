The spec and the oracle allow comparing a record and a string with the equal (and notequal) function. My program forbade this, and would return FAILED. These comparisons have been allowed.
