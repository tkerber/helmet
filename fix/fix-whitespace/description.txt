The spec allows for whitespace in front of commands and the program terminator ***. My server did not allow whitespace in these locations (expect in front of the first command). This has been added.
