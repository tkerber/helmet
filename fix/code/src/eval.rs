use std::collections::HashMap;
use std::fmt::{self, Display};
use state::{Value, State, Transaction};
use parse::{self, Program, Expr, PrimitiveCommand, Func, FinalCommand, Right};
use nom;
use symbol::{lookup, Symbol, ADMIN};

// Note: The lossy part of this conversion should never happen, as all identifiers and strings must
// be valid UTF-8 to be parsed.
fn stringify(bytes: &[u8]) -> String {
    String::from_utf8_lossy(bytes).into_owned()
}

#[derive(Debug)]
pub enum Status {
    Failed,
    Denied,
    Exiting,
    Returning(Value),
    CreatePrincipal,
    ChangePassword,
    Set,
    Append,
    Local,
    ForEach,
    SetDelegation,
    DeleteDelegation,
    DefaultDelegator,
    FilterEach,
    Timeout,
}

impl Display for Status {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let status = match self {
            &Status::Failed => "FAILED",
            &Status::Denied => "DENIED",
            &Status::Exiting => "EXITING",
            &Status::Returning(_) => "RETURNING",
            &Status::CreatePrincipal => "CREATE_PRINCIPAL",
            &Status::ChangePassword => "CHANGE_PASSWORD",
            &Status::Set => "SET",
            &Status::Append => "APPEND",
            &Status::Local => "LOCAL",
            &Status::ForEach => "FOREACH",
            &Status::SetDelegation => "SET_DELEGATION",
            &Status::DeleteDelegation => "DELETE_DELEGATION",
            &Status::DefaultDelegator => "DEFAULT_DELEGATOR",
            &Status::FilterEach => "FILTEREACH",
            &Status::Timeout => "TIMEOUT",
        };
        try!(fmt.write_fmt(format_args!(r##"{{"status":"{}""##, status)));
        if let &Status::Returning(ref val) = self {
            fmt.write_fmt(format_args!(r##","output":{}}}"##, val))
        } else {
            fmt.write_str("}")
        }
    }
}

#[derive(Debug)]
pub enum ExecutionError {
    Login,
    SecurityViolation,
    DuplicateFieldName,
    NotARecord,
    NotAList,
    NotAString,
    UnexpectedArgs,
    FieldNotFound,
    VariableNotFound,
    PrincipalNotFound,
    PrincipalAlreadyExists,
    WouldShadow,
    EqNotAllowed,
}

pub trait Eval {
    type Return;
    fn eval<'a>(&self,
                transaction: &mut Transaction<'a>,
                statuses: &mut Vec<Status>,
                local_scope: &mut HashMap<Symbol, Value>)
                -> Result<Self::Return, ExecutionError>;
}

struct FunctionCall<'a> {
    func: Func,
    vals: &'a [parse::Value<'a>],
}

impl<'a> FunctionCall<'a> {
    fn new(func: Func, vals: &'a [parse::Value]) -> Self {
        FunctionCall {
            func: func,
            vals: vals,
        }
    }
}

impl<'a> Eval for FunctionCall<'a> {
    type Return = Value;

    fn eval<'b>(&self,
                transaction: &mut Transaction<'b>,
                statuses: &mut Vec<Status>,
                local_scope: &mut HashMap<Symbol, Value>)
                -> Result<Self::Return, ExecutionError> {
        match (self.vals.len(), self.func) {
            (1, Func::ToLower) |
            (2, Func::Split) |
            (2, Func::Concat) |
            (2, Func::Equal) |
            (2, Func::NotEqual) => {},
            _ => return Err(ExecutionError::UnexpectedArgs),
        }
        let v1 = try!(self.vals[0].eval(transaction, statuses, local_scope));
        let v2 = if self.vals.len() >= 2 {
            Some(try!(self.vals[1].eval(transaction, statuses, local_scope)))
        } else {
            None
        };
        match (self.func, v1, v2) {
            (Func::Split, Value::String(s1), Some(Value::String(s2))) => {
                if s2.len() > s1.len() {
                    Ok(Value::Record(vec![
                        ("fst".to_owned(), s1),
                        ("snd".to_owned(), String::new()),
                    ]))
                } else {
                    let (s1, s2) = s1.split_at(s2.len());
                    Ok(Value::Record(vec![
                        ("fst".to_owned(), s1.to_owned()),
                        ("snd".to_owned(), s2.to_owned()),
                    ]))
                }
            }
            (Func::Concat, Value::String(mut s1), Some(Value::String(s2))) => {
                s1.push_str(&s2);
                s1.truncate(65535);
                Ok(Value::String(s1))
            }
            (Func::ToLower, Value::String(s), None) => Ok(Value::String(s.to_lowercase())),
            (Func::Equal, v1, Some(v2)) => {
                Value::eq(v1, v2).map(|b| if b {
                    Value::String("".to_owned())
                } else {
                    Value::String("0".to_owned())
                })
            }
            (Func::NotEqual, v1, Some(v2)) => {
                Value::eq(v1, v2).map(|b| if b {
                    Value::String("0".to_owned())
                } else {
                    Value::String("".to_owned())
                })
            }
            _ => return Err(ExecutionError::UnexpectedArgs),
        }
    }
}

impl<'a> Eval for Expr<'a> {
    type Return = Value;

    fn eval<'b>(&self,
                transaction: &mut Transaction<'b>,
                statuses: &mut Vec<Status>,
                local_scope: &mut HashMap<Symbol, Value>)
                -> Result<Self::Return, ExecutionError> {
        match self {
            &Expr::Value(ref val) => val.eval(transaction, statuses, local_scope),
            &Expr::EmptyArray => Ok(Value::List(Vec::new())),
            &Expr::Field(ref list) => {
                let mut rec = Vec::new();
                for &(ref field, ref val) in list {
                    let val = try!(try!(val.eval(transaction, statuses, local_scope)).require_str());
                    let fieldname = stringify(field);
                    // Sanity check: the field may not have existed previously.
                    if rec.iter().filter(|&&(ref field, _)| field == &fieldname).next().is_some() {
                        return Err(ExecutionError::DuplicateFieldName);
                    }
                    rec.push((fieldname, val));
                }
                Ok(Value::Record(rec))
            }
            &Expr::FuncCall(func, ref vals) => {
                FunctionCall::new(func, &vals[..]).eval(transaction, statuses, local_scope)
            }
            &Expr::Let(var, ref expr1, ref expr2) => {
                let var = lookup(var);
                // Check if the iterator variable exists.
                if let Err(ExecutionError::VariableNotFound) = transaction.get(var, local_scope) {
                } else {
                    return Err(ExecutionError::WouldShadow);
                }
                let val = try!(expr1.eval(transaction, statuses, local_scope));
                local_scope.insert(var.clone(), val);
                let res = expr2.eval(transaction, statuses, local_scope);
                local_scope.remove(&var);
                res
            }
        }
    }
}

impl<'a> Eval for parse::Value<'a> {
    type Return = Value;

    fn eval<'b>(&self,
                transaction: &mut Transaction<'b>,
                _: &mut Vec<Status>,
                local_scope: &mut HashMap<Symbol, Value>)
                -> Result<Self::Return, ExecutionError> {
        Ok(match self {
            &parse::Value::Variable(var) => {
                let var = lookup(var);
                try!(transaction.require_right(var, Right::Read, local_scope));
                try!(transaction.get(var, local_scope)).into_owned()
            }
            &parse::Value::Accessor(var, member) => {
                let var = lookup(var);
                try!(transaction.require_right(var, Right::Read, local_scope));
                Value::String(try!(try!(transaction.get(var, local_scope))
                        .get_record(&stringify(member)))
                    .to_owned())
            }
            &parse::Value::String(bytes) => Value::String(stringify(bytes)),
        })
    }
}

impl<'a> Eval for PrimitiveCommand<'a> {
    type Return = ();

    fn eval<'b>(&self,
                transaction: &mut Transaction<'b>,
                statuses: &mut Vec<Status>,
                local_scope: &mut HashMap<Symbol, Value>)
                -> Result<Self::Return, ExecutionError> {
        match self {
            &PrimitiveCommand::CreatePrincipal(principal, password) => {
                try!(transaction.create_principal(lookup(principal), stringify(password)));
                statuses.push(Status::CreatePrincipal);
            }
            &PrimitiveCommand::ChangePassword(principal, password) => {
                try!(transaction.change_password(lookup(principal), stringify(password)));
                statuses.push(Status::ChangePassword);
            }
            &PrimitiveCommand::Set(ref variable, ref expr) => {
                let variable = lookup(variable);
                let val = try!(expr.eval(transaction, statuses, local_scope));
                try!(transaction.require_right(variable, Right::Write, local_scope));
                try!(transaction.set(variable, local_scope, val));
                statuses.push(Status::Set);
            }
            &PrimitiveCommand::Append(ref variable, ref expr) => {
                let variable = lookup(variable);
                try!(transaction.require_right(variable, Right::Append, &local_scope)
                    .or_else(|_| transaction.require_right(variable, Right::Write, &local_scope)));
                try!(transaction.append(variable, local_scope, statuses, expr));
                statuses.push(Status::Append);
            }
            &PrimitiveCommand::Local(ref variable, ref expr) => {
                let variable = lookup(variable);
                try!(transaction.set_local(variable, local_scope, statuses, expr));
                statuses.push(Status::Local);
            }
            &PrimitiveCommand::Foreach(ref iterator, ref iterable, ref expr) => {
                let (iterator, iterable) = (lookup(iterator), lookup(iterable));
                try!(transaction.require_right(iterable, Right::Read, &local_scope));
                try!(transaction.require_right(iterable, Right::Write, &local_scope));
                // Get the iterable value
                let val = try!(transaction.get(iterable, local_scope)).into_owned();
                // Check if the iterator variable exists.
                if let Err(ExecutionError::VariableNotFound) =
                       transaction.get(iterator, local_scope) {
                } else {
                    return Err(ExecutionError::WouldShadow);
                }
                let mut res = Vec::new();
                match val {
                    Value::List(list) => {
                        for item in list {
                            local_scope.insert(iterator.clone(), item);
                            res.push(try!(expr.eval(transaction, statuses, local_scope)));
                        }
                        local_scope.remove(&iterator);
                    }
                    _ => return Err(ExecutionError::NotAList),
                }
                // Set the new value.
                try!(transaction.set(iterable, local_scope, Value::List(res)));
                statuses.push(Status::ForEach);
            }
            &PrimitiveCommand::SetDelegation(ref variable, ref delegator, right, ref delegee) => {
                try!(transaction.set_delegation(lookup(variable),
                                                lookup(delegator),
                                                right,
                                                lookup(delegee)));
                statuses.push(Status::SetDelegation);
            }
            &PrimitiveCommand::DeleteDelegation(variable, delegator, right, delegee) => {
                try!(transaction.delete_delegation(lookup(variable),
                                                   lookup(delegator),
                                                   right,
                                                   lookup(delegee)));
                statuses.push(Status::DeleteDelegation);
            }
            &PrimitiveCommand::DefaultDelegator(delegator) => {
                try!(transaction.set_default_delegator(lookup(delegator)));
                statuses.push(Status::DefaultDelegator);
            }
            &PrimitiveCommand::FilterEach(ref iterator, ref iterable, ref expr) => {
                let (iterator, iterable) = (lookup(iterator), lookup(iterable));
                try!(transaction.require_right(iterable, Right::Read, local_scope));
                try!(transaction.require_right(iterable, Right::Write, local_scope));
                // Get the iterable value
                let val = try!(transaction.get(iterable, local_scope)).into_owned();
                // Check if the iterator variable exists.
                if let Err(ExecutionError::VariableNotFound) =
                       transaction.get(iterator, local_scope) {
                } else {
                    return Err(ExecutionError::WouldShadow);
                }
                let mut res = Vec::new();
                match val {
                    Value::List(list) => {
                        for item in list {
                            local_scope.insert(iterator.clone(), item.clone());
                            match try!(expr.eval(transaction, statuses, local_scope)) {
                                Value::String(ref s) if s == "" => res.push(item),
                                _ => {}
                            }
                        }
                        local_scope.remove(&iterator);
                    }
                    _ => return Err(ExecutionError::NotAList),
                }
                // Set the new value.
                try!(transaction.set(iterable, local_scope, Value::List(res)));
                statuses.push(Status::FilterEach);
            }
        }
        Ok(())
    }
}

impl<'a> Eval for FinalCommand<'a> {
    type Return = ();

    fn eval<'b>(&self,
                transaction: &mut Transaction<'b>,
                statuses: &mut Vec<Status>,
                local_scope: &mut HashMap<Symbol, Value>)
                -> Result<Self::Return, ExecutionError> {
        match self {
            &FinalCommand::Exit => {
                return if transaction.principal() == ADMIN {
                    statuses.push(Status::Exiting);
                    Ok(())
                } else {
                    Err(ExecutionError::SecurityViolation)
                };
            }
            &FinalCommand::Return(ref expr) => {
                let val = try!(expr.eval(transaction, statuses, local_scope));
                statuses.push(Status::Returning(val));
                return Ok(());
            }
        }
    }
}

pub fn eval<'a>(prog: &[u8], state: &'a mut State) -> (Vec<Status>, Option<Transaction<'a>>) {
    // Short circuit to prevent the parsed from doing complex operations on inputs that are too
    // long anyway.
    if prog.len() > 1000000 {
        return (vec![Status::Failed], None);
    }
    match Program::parse(prog) {
        nom::IResult::Done(_, prog) => {
            match eval_program(prog, state) {
                Ok((statuses, transaction)) => (statuses, Some(transaction)),
                Err(ExecutionError::SecurityViolation) => (vec![Status::Denied], None),
                Err(_) => (vec![Status::Failed], None),
            }
        }
        _ => (vec![Status::Failed], None),
    }
}

pub fn eval_program<'a>(prog: Program, state: &'a mut State) -> Result<(Vec<Status>, Transaction<'a>), ExecutionError> {
    let mut transaction =
        try!(state.transaction(lookup(prog.principal), stringify(prog.password).as_ref()));
    let mut statuses = Vec::new();
    let command = prog.command;
    let mut local_scope = HashMap::new();
    for cmd in command.0 {
        try!(cmd.eval(&mut transaction, &mut statuses, &mut local_scope));
    }
    try!(command.1.eval(&mut transaction, &mut statuses, &mut local_scope));
    Ok((statuses, transaction))
}
