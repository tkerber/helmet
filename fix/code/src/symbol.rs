
use std::cell::RefCell;
use std::collections::HashMap;

pub type Symbol = usize;

pub const ADMIN: Symbol = 0;
pub const ANYONE: Symbol = 1;
pub const ALL: Symbol = 2;

#[derive(Debug)]
pub struct SymbolTable {
    table: HashMap<Vec<u8>, Symbol>,
    next: Symbol,
}

impl SymbolTable {
    fn new() -> Self {
        let mut table = HashMap::new();
        table.insert((&b"admin"[..]).to_owned(), 0);
        table.insert((&b"anyone"[..]).to_owned(), 1);
        table.insert((&b"all"[..]).to_owned(), 2);
        SymbolTable {
            table: table,
            next: 3,
        }
    }
}

thread_local! {
    pub static SYMBOL_TABLE: RefCell<SymbolTable> = RefCell::new(SymbolTable::new());
}

pub fn lookup(bytes: &[u8]) -> Symbol {
    SYMBOL_TABLE.with(|t| {
        if let Some(s) = t.borrow().table.get::<[u8]>(bytes) {
            return *s;
        }
        let mut table = t.borrow_mut();
        let key = bytes.to_owned();
        let sym = table.next;
        table.table.insert(key, sym);
        table.next = sym + 1;
        sym
    })
}
