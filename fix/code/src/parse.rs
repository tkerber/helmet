use std::collections::LinkedList;
use nom;

const ERR_RESERVED: u32 = 0;
const ERR_TOO_LONG: u32 = 1;
const ERR_EXPECTED_EOF: u32 = 2;
const ERR_IDENT: u32 = 3;
const ERR_STRING: u32 = 4;

const COMMENT_CHARS: &'static [u8] =
    b"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_ ,;.?-!";
const IDENT_FIRST: &'static [u8] = b"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
const IDENT: &'static [u8] = b"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
pub const STRING: &'static [u8] =
    b"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_ ,;.?-!";

const KEYWORDS: &'static [&'static [u8]] = &[b"all",
                                             b"append",
                                             b"as",
                                             b"change",
                                             b"create",
                                             b"default",
                                             b"delegate",
                                             b"delegation",
                                             b"delegator",
                                             b"delete",
                                             b"do",
                                             b"exit",
                                             b"foreach",
                                             b"in",
                                             b"local",
                                             b"password",
                                             b"principal",
                                             b"read",
                                             b"replacewith",
                                             b"return",
                                             b"set",
                                             b"to",
                                             b"write",
                                             b"split",
                                             b"concat",
                                             b"tolower",
                                             b"notequal",
                                             b"equal",
                                             b"filtereach",
                                             b"with",
                                             b"let"];

#[derive(Debug)]
pub struct Program<'a> {
    pub principal: &'a [u8],
    pub password: &'a [u8],
    pub command: Command<'a>,
}

impl<'b> Program<'b> {
    pub fn parse(b: &[u8]) -> nom::IResult<&[u8], Program> {
        if b.len() > 1000000 {
            return nom::IResult::Error(nom::Err::Code(nom::ErrorKind::Custom(ERR_TOO_LONG)));
        }
        let (rem, res) = try_parse!(b,
                                    chain!(
            many0!(chain!(
                comment ~
                char!('\n'),
                || ())) ~
            whitespace0 ~
            tag!(b"as") ~
            whitespace1 ~
            tag!(b"principal") ~
            whitespace1 ~
            principal: ident ~
            whitespace1 ~
            tag!(b"password") ~
            whitespace0 ~
            password: string ~
            whitespace0 ~
            tag!(b"do") ~
            lineend ~
            command: call!(Command::parse) ~
            whitespace0 ~
            tag!(b"***"),
            || {
                Program {
                    principal: principal,
                    password: password,
                    command: command,
                }
            }
        ));
        if rem.len() != 0 {
            nom::IResult::Error(nom::Err::Position(nom::ErrorKind::Custom(ERR_EXPECTED_EOF), rem))
        } else {
            nom::IResult::Done(rem, res)
        }
    }
}

#[derive(Debug)]
pub enum FinalCommand<'a> {
    Exit,
    Return(Expr<'a>),
}

#[derive(Debug)]
pub struct Command<'a>(pub Vec<PrimitiveCommand<'a>>, pub FinalCommand<'a>);

impl<'b> Command<'b> {
    named!(pub parse<Command>, chain!(
        pcmds: many0!(chain!(
            whitespace0 ~
            pcmd: call!(PrimitiveCommand::parse) ~
            lineend,
            || pcmd)) ~
        whitespace0 ~
        final_cmd: alt_complete!(
            chain!(
                tag!(b"exit") ~
                lineend,
                || FinalCommand::Exit) |
            chain!(
                tag!(b"return") ~
                whitespace1 ~
                expr: call!(Expr::parse) ~
                lineend,
                || FinalCommand::Return(expr))),
        || Command(pcmds, final_cmd)
    ));
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Func {
    Split,
    Concat,
    ToLower,
    Equal,
    NotEqual,
}

impl Func {
    named!(parse<Func>, alt_complete!(
        value!(Func::Split, tag!(b"split")) |
        value!(Func::Concat, tag!(b"concat")) |
        value!(Func::ToLower, tag!(b"tolower")) |
        value!(Func::Equal, tag!(b"equal")) |
        value!(Func::NotEqual, tag!(b"notequal"))
    ));
}

#[derive(Debug)]
pub enum Expr<'a> {
    Value(Value<'a>),
    EmptyArray,
    Field(LinkedList<(&'a [u8], Value<'a>)>),
    FuncCall(Func, Vec<Value<'a>>),
    Let(&'a [u8], Box<Expr<'a>>, Box<Expr<'a>>),
}

impl<'b> Expr<'b> {
    named!(parse_fieldvals< LinkedList<(&[u8], Value)> >, chain!(
        var: ident ~
        whitespace0 ~
        char!('=') ~
        whitespace0 ~
        val: call!(Value::parse) ~
        optlist: opt!(chain!(
            whitespace0 ~
            char!(',') ~
            whitespace0 ~
            list: call!(Expr::parse_fieldvals),
            || list)),
        || {
            let mut list = optlist.unwrap_or_else(|| LinkedList::new());
            list.push_front((var, val));
            list
        }
    ));

    named!(parse<Expr>, alt_complete!(
        value!(Expr::EmptyArray, tag!(b"[]")) |
        chain!(
            char!('{') ~
            whitespace0 ~
            vals: call!(Expr::parse_fieldvals) ~
            whitespace0 ~
            char!('}'),
            || Expr::Field(vals)) |
        map!(call!(Value::parse), |val| Expr::Value(val)) |
        chain!(
            func: call!(Func::parse) ~
            whitespace0 ~
            char!('(') ~
            whitespace0 ~
            val1: call!(Value::parse) ~
            whitespace0 ~
            vals: many0!(chain!(
                char!(',') ~
                whitespace0 ~
                val: call!(Value::parse),
                || val)) ~
            whitespace0 ~
            char!(')'),
            || {
                let mut args = Vec::new();
                args.push(val1);
                args.extend(vals);
                Expr::FuncCall(func, args)
            }) |
        chain!(
            tag!(b"let") ~
            whitespace1 ~
            ident: ident ~
            whitespace0 ~
            char!('=') ~
            whitespace0 ~
            expr1: call!(Expr::parse) ~
            whitespace1 ~
            tag!(b"in") ~
            whitespace1 ~
            expr2: call!(Expr::parse),
            || Expr::Let(ident, Box::new(expr1), Box::new(expr2)))
    ));
}

#[derive(Debug)]
pub enum Value<'a> {
    Variable(&'a [u8]),
    Accessor(&'a [u8], &'a [u8]),
    String(&'a [u8]),
}

impl<'b> Value<'b> {
    named!(parse<Value>, alt_complete!(
        map!(string, |s| Value::String(s)) |
        chain!(
            i1: ident ~
            whitespace0 ~
            char!('.') ~
            whitespace0 ~
            i2: ident,
            || Value::Accessor(i1, i2)) |
        map!(ident, |i| Value::Variable(i))
    ));
}

#[derive(Debug)]
pub enum PrimitiveCommand<'a> {
    CreatePrincipal(&'a [u8], &'a [u8]),
    ChangePassword(&'a [u8], &'a [u8]),
    Set(&'a [u8], Expr<'a>),
    Append(&'a [u8], Expr<'a>),
    Local(&'a [u8], Expr<'a>),
    Foreach(&'a [u8], &'a [u8], Expr<'a>),
    SetDelegation(&'a [u8], &'a [u8], Right, &'a [u8]),
    DeleteDelegation(&'a [u8], &'a [u8], Right, &'a [u8]),
    DefaultDelegator(&'a [u8]),
    FilterEach(&'a [u8], &'a [u8], Expr<'a>),
}

impl<'b> PrimitiveCommand<'b> {
    named!(parse<PrimitiveCommand>, alt_complete!(
        chain!(
            tag!(b"create") ~
            whitespace1 ~
            tag!(b"principal") ~
            whitespace1 ~
            ident: ident ~
            whitespace0 ~
            string: string,
            || PrimitiveCommand::CreatePrincipal(ident, string)) |
        chain!(
            tag!(b"change") ~
            whitespace1 ~
            tag!(b"password") ~
            whitespace1 ~
            ident: ident ~
            whitespace0 ~
            string: string,
            || PrimitiveCommand::ChangePassword(ident, string)) |
        chain!(
            tag!(b"set") ~
            whitespace1 ~
            ident: ident ~
            whitespace0 ~
            char!('=') ~
            whitespace0 ~
            expr: call!(Expr::parse),
            || PrimitiveCommand::Set(ident, expr)) |
        chain!(
            tag!(b"append") ~
            whitespace1 ~
            tag!(b"to") ~
            whitespace1 ~
            ident: ident ~
            whitespace1 ~
            tag!(b"with") ~
            whitespace1 ~
            expr: call!(Expr::parse),
            || PrimitiveCommand::Append(ident, expr)) |
        chain!(
            tag!(b"local") ~
            whitespace1 ~
            ident: ident ~
            whitespace0 ~
            char!('=') ~
            whitespace0 ~
            expr: call!(Expr::parse),
            || PrimitiveCommand::Local(ident, expr)) |
        chain!(
            tag!(b"foreach") ~
            whitespace1 ~
            i1: ident ~
            whitespace1 ~
            tag!(b"in") ~
            whitespace1 ~
            i2: ident ~
            whitespace1 ~
            tag!(b"replacewith") ~
            whitespace1 ~
            expr: call!(Expr::parse),
            || PrimitiveCommand::Foreach(i1, i2, expr)) |
        chain!(
            tag!(b"set") ~
            whitespace1 ~
            tag!(b"delegation") ~
            whitespace1 ~
            i1: alt_complete!(tag!(b"all") | ident) ~
            whitespace1 ~
            i2: ident ~
            whitespace1 ~
            right: call!(Right::parse) ~
            whitespace0 ~
            tag!(b"->") ~
            whitespace0 ~
            i3: ident,
            || PrimitiveCommand::SetDelegation(i1, i2, right, i3)) |
        chain!(
            tag!(b"delete") ~
            whitespace1 ~
            tag!(b"delegation") ~
            whitespace1 ~
            i1: alt_complete!(tag!(b"all") | ident) ~
            whitespace1 ~
            i2: ident ~
            whitespace1 ~
            right: call!(Right::parse) ~
            whitespace0 ~
            tag!(b"->") ~
            whitespace0 ~
            i3: ident,
            || PrimitiveCommand::DeleteDelegation(i1, i2, right, i3)) |
        chain!(
            tag!(b"default") ~
            whitespace1 ~
            tag!(b"delegator") ~
            whitespace0 ~
            char!('=') ~
            whitespace0 ~
            ident: ident,
            || PrimitiveCommand::DefaultDelegator(ident)) |
        chain!(
            tag!(b"filtereach") ~
            whitespace1 ~
            iterator: ident ~
            whitespace1 ~
            tag!(b"in") ~
            whitespace1 ~
            iterable: ident ~
            whitespace1 ~
            tag!(b"with") ~
            whitespace1 ~
            expr: call!(Expr::parse),
            || PrimitiveCommand::FilterEach(iterator, iterable, expr))
    ));
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
pub enum Right {
    Read,
    Write,
    Append,
    Delegate,
}

impl Right {
    named!(parse<Right>, alt_complete!(
        value!(Right::Read, tag!(b"read")) |
        value!(Right::Write, tag!(b"write")) |
        value!(Right::Append, tag!(b"append")) |
        value!(Right::Delegate, tag!(b"delegate"))
    ));
}

named!(whitespace0<()>, value!((), many0!(char!(' '))));

named!(whitespace1<()>, value!((), many1!(char!(' '))));

named!(comment<()>, chain!(
    tag!("//") ~
    many0!(one_of!(COMMENT_CHARS)),
    || ()));

named!(lineend<()>, chain!(
    whitespace0 ~
    opt!(comment) ~
    char!('\n') ~
    many0!(chain!(
        comment ~
        char!('\n'),
        || ())),
    || ()));

fn ident(b: &[u8]) -> nom::IResult<&[u8], &[u8]> {
    if b.len() == 0 || !IDENT_FIRST.contains(&b[0]) {
        return nom::IResult::Error(nom::Err::Position(nom::ErrorKind::Custom(ERR_IDENT), b));
    }
    let mut i = 1;
    while b.len() > i && IDENT.contains(&b[i]) {
        i += 1;
    }
    let (rem, res) = (&b[i..], &b[..i]);
    if KEYWORDS.contains(&res) {
        nom::IResult::Error(nom::Err::Position(nom::ErrorKind::Custom(ERR_RESERVED), rem))
    } else if res.len() > 255 {
        nom::IResult::Error(nom::Err::Position(nom::ErrorKind::Custom(ERR_TOO_LONG), rem))
    } else {
        nom::IResult::Done(rem, res)
    }
}

fn string(b: &[u8]) -> nom::IResult<&[u8], &[u8]> {
    if b.len() == 0 || b[0] != '"' as u8 {
        return nom::IResult::Error(nom::Err::Position(nom::ErrorKind::Custom(ERR_STRING), b));
    }
    let mut i = 1;
    while b.len() > i && STRING.contains(&b[i]) {
        i += 1;
    }
    if b.len() <= i || b[i] != '"' as u8 {
        return nom::IResult::Error(nom::Err::Position(nom::ErrorKind::Custom(ERR_STRING), &b[i..]));
    }
    let (rem, res) = (&b[i + 1..], &b[1..i]);
    if res.len() > 65535 {
        nom::IResult::Error(nom::Err::Position(nom::ErrorKind::Custom(ERR_TOO_LONG), rem))
    } else {
        nom::IResult::Done(rem, res)
    }
}
