
use std::cell::RefCell;
use std::collections::{HashMap, HashSet};
use std::borrow::Cow;
use std::fmt::{self, Display};
use std::hash::Hash;
use parse::Right;
use eval::{ExecutionError, Eval, Status};
use symbol::{Symbol, ADMIN, ANYONE, ALL};

/// It is easier to just give anyone a password than have it be the only principal without one.
/// Fortunately, strings have a limited character set, so I can give anyone an impossible password,
/// so that breaker teams (boo! hiss!) can't login as it.
const ANYONE_PASS: &'static str = "***";

#[derive(Clone, Debug)]
pub enum Value {
    String(String),
    List(Vec<Value>),
    Record(Vec<(String, String)>),
}

type HashMapList<T, U> = HashMap<T, Vec<U>>;

trait HashMapListExt<T, U> {
    fn push(&mut self, key: T, value: U);
    fn extend_hashmap_list<V: IntoIterator<Item = U>>(&mut self, key: T, iter: V);
    fn contains(&self, key: &T, value: &U) -> bool where U: PartialEq<U>;
    fn remove_list(&mut self, key: &T, value: &U) where U: PartialEq<U>;
}

trait HashMapListAppend<T> {
    fn append(&mut self, key: T, value: Value);
}

impl<T, U> HashMapListExt<T, U> for HashMapList<T, U>
    where T: Hash + Eq
{
    fn push(&mut self, key: T, value: U) {
        if self.contains_key(&key) {
            // Safe as self contains &key.
            self.get_mut(&key).unwrap().push(value);
        } else {
            self.insert(key, vec![value]);
        }
    }

    fn extend_hashmap_list<V: IntoIterator<Item = U>>(&mut self, key: T, iter: V) {
        if self.contains_key(&key) {
            // Safe as self contains &key.
            self.get_mut(&key).unwrap().extend(iter);
        } else {
            self.insert(key, iter.into_iter().collect());
        }
    }

    fn contains(&self, key: &T, value: &U) -> bool
        where U: PartialEq<U>
    {
        if let Some(v) = self.get(&key) {
            v.contains(value)
        } else {
            false
        }
    }

    fn remove_list(&mut self, key: &T, value: &U)
        where U: PartialEq<U>
    {
        if let Some(l) = self.get_mut(key) {
            l.retain(|v| v != value);
        }
    }
}

impl<T> HashMapListAppend<T> for HashMapList<T, Value>
    where T: Hash + Eq
{
    fn append(&mut self, key: T, value: Value) {
        match value {
            Value::List(l) => self.extend_hashmap_list(key, l),
            v => self.push(key, v),
        }
    }
}

trait HashMapValueExt<T> {
    fn append(&mut self, key: &T, value: Value) -> Result<(), ExecutionError>;
}

impl<T> HashMapValueExt<T> for HashMap<T, Value>
    where T: Hash + Eq
{
    fn append(&mut self, key: &T, mut value: Value) -> Result<(), ExecutionError> {
        match self.get_mut(key) {
            Some(&mut Value::List(ref mut l)) => {
                match value {
                    Value::List(ref mut l2) => l.append(l2),
                    v => l.push(v),
                }
                Ok(())
            }
            Some(_) => Err(ExecutionError::NotAList),
            None => Err(ExecutionError::VariableNotFound),
        }
    }
}

impl Value {
    pub fn eq(self, other: Self) -> Result<bool, ExecutionError> {
        match (self, other) {
            (Value::String(s1), Value::String(s2)) => Ok(s1 == s2),
            (Value::Record(mut r1), Value::Record(mut r2)) => {
                if r1.len() != r2.len() {
                    return Ok(false);
                }
                r1.sort();
                r2.sort();
                Ok(r1 == r2)
            }
            (Value::String(_), Value::Record(_)) |
            (Value::Record(_), Value::String(_)) => Ok(false),
            _ => Err(ExecutionError::EqNotAllowed),
        }
    }

    pub fn require_str(self) -> Result<String, ExecutionError> {
        match self {
            Value::String(s) => Ok(s),
            _ => Err(ExecutionError::NotAString),
        }
    }

    pub fn as_list(self) -> Vec<Value> {
        match self {
            Value::List(v) => v,
            _ => panic!("not a list"),
        }
    }

    pub fn get_record(&self, key: &str) -> Result<&str, ExecutionError> {
        if let &Value::Record(ref vec) = self {
            for &(ref k, ref v) in vec.iter() {
                if k == key {
                    return Ok(v);
                }
            }
            Err(ExecutionError::FieldNotFound)
        } else {
            Err(ExecutionError::NotARecord)
        }
    }
}

impl Display for Value {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Value::String(ref s) => fmt.write_fmt(format_args!(r##""{}""##, s)),
            &Value::List(ref l) => {
                try!(fmt.write_str("["));
                let mut first = true;
                for item in l {
                    if first {
                        first = false;
                    } else {
                        try!(fmt.write_str(","));
                    }
                    try!(item.fmt(fmt));
                }
                fmt.write_str("]")
            }
            &Value::Record(ref items) => {
                try!(fmt.write_str("{"));
                let mut first = true;
                for &(ref k, ref v) in items {
                    if first {
                        first = false;
                    } else {
                        try!(fmt.write_str(","));
                    }
                    try!(fmt.write_fmt(format_args!(r##""{}":"{}""##, k, v)));
                }
                fmt.write_str("}")
            }
        }
    }
}

/// Contains the system state. This cannot be accessed directly, but can only be used through a
/// transaction.
#[derive(Debug)]
pub struct State {
    /// Hashmap from username to password.
    ///
    /// Allows for fast membership tests, and fast retrieval of password, even with many
    /// principals.
    principals: HashMap<Symbol, String>,
    /// Hashmap from variable name to value.
    globals: HashMap<Symbol, Value>,
    /// Hashmap from (delegee, variable, right) to a list of delegators.
    delegations: HashMapList<(Symbol, Symbol, Right), Symbol>,
    default_delegator: Symbol,
}

impl State {
    pub fn new(admin_pass: Option<String>) -> Self {
        let mut principals = HashMap::new();
        principals.insert(ADMIN, admin_pass.unwrap_or_else(|| "admin".to_owned()));
        principals.insert(ANYONE, ANYONE_PASS.to_owned());
        State {
            principals: principals,
            globals: HashMap::new(),
            delegations: HashMap::new(),
            default_delegator: ANYONE,
        }
    }

    pub fn transaction<'a>(&'a mut self,
                           principal: Symbol,
                           password: &str)
                           -> Result<Transaction<'a>, ExecutionError> {
        {
            let pass = match self.principals.get(&principal) {
                Some(v) => v.as_str(),
                None => return Err(ExecutionError::Login),
            };
            if pass != password {
                return Err(ExecutionError::SecurityViolation);
            }
        }
        Ok(Transaction {
            state: self,
            principal: principal,
            rights_cache: RefCell::new(HashSet::new()),
            delegations_added: HashMap::new(),
            delegations_removed: HashMap::new(),
            default_delegator_set: None,
            principal_modified: HashMap::new(),
            value_set: HashMap::new(),
            value_appended: HashMap::new(),
        })
    }
}

pub enum DelegationState {
    Exists,
    Removed,
    Added,
    Nonexistant,
}

#[derive(PartialEq, Eq, Clone, Copy)]
pub enum VariableState {
    Local,
    Set,
    Appended,
    Unchanged,
    NotFound,
}

pub struct Transaction<'a> {
    state: &'a mut State,
    principal: Symbol,
    rights_cache: RefCell<HashSet<(Symbol, Symbol, Right)>>,
    delegations_added: HashMapList<(Symbol, Symbol, Right), Symbol>,
    delegations_removed: HashMapList<(Symbol, Symbol, Right), Symbol>,
    default_delegator_set: Option<Symbol>,
    principal_modified: HashMap<Symbol, String>,
    value_set: HashMap<Symbol, Value>,
    value_appended: HashMapList<Symbol, Value>,
}

impl<'a> Transaction<'a> {
    fn variable_state(&self,
                      var: Symbol,
                      local_scope: Option<&HashMap<Symbol, Value>>)
                      -> VariableState {
        if let Some(local_scope) = local_scope {
            if local_scope.contains_key(&var) {
                return VariableState::Local;
            }
        }
        if self.value_set.contains_key(&var) {
            return VariableState::Set;
        }
        if self.value_appended.contains_key(&var) {
            return VariableState::Appended;
        }
        if self.state.globals.contains_key(&var) {
            return VariableState::Unchanged;
        }
        VariableState::NotFound
    }

    fn default_delegator(&self) -> Symbol {
        self.default_delegator_set.unwrap_or(self.state.default_delegator)
    }

    fn principal_exists(&self, principal: Symbol) -> bool {
        self.principal_modified.contains_key(&principal) ||
        self.state.principals.contains_key(&principal)
    }

    fn delegation_state(&self,
                        accessor: &(Symbol, Symbol, Right),
                        delegator: &Symbol)
                        -> DelegationState {
        let exists_state = self.state.delegations.contains(&accessor, delegator);
        let was_removed = self.delegations_removed.contains(&accessor, delegator);
        let was_added = self.delegations_added.contains(&accessor, delegator);
        match (exists_state, was_removed, was_added) {
            (true, false, false) => DelegationState::Exists,
            (true, true, false) => DelegationState::Removed,
            (false, false, true) => DelegationState::Added,
            (false, false, false) => DelegationState::Nonexistant,
            _ => unreachable!(),
        }
    }

    pub fn principal(&self) -> Symbol {
        self.principal
    }

    fn variables(&self) -> Vec<Symbol> {
        let mut variables = self.state
            .globals
            .keys()
            .chain(self.value_set.keys())
            .map(|v| v.clone())
            .collect::<Vec<_>>();
        variables.sort();
        variables.dedup();
        variables
    }

    fn delegate(&mut self, variable: Symbol, delegator: Symbol, delegee: Symbol, right: Right) {
        if variable == ALL {
            for variable in self.variables() {
                assert!(variable != ALL);
                if self.has_right(delegator, variable, Right::Delegate) {
                    self.delegate(variable, delegator, delegee, right);
                }
            }
            return;
        }
        let accessor = (variable, delegee, right);
        let state = self.delegation_state(&accessor, &delegator);
        match state {
            DelegationState::Removed => {
                // If the delegation was removed in this transaction, unremove it.
                // Unwrap safe due to delegation state.
                self.delegations_removed.get_mut(&accessor).unwrap().retain(|v| v != &delegator);
            }
            DelegationState::Added | DelegationState::Exists => {}
            DelegationState::Nonexistant => {
                self.delegations_added.push(accessor, delegator);
            }
        }
    }

    fn undelegate(&mut self, variable: Symbol, delegator: Symbol, delegee: Symbol, right: Right) {
        if variable == ALL {
            for variable in self.variables() {
                assert!(variable != ALL);
                if self.has_right(delegator, variable, Right::Delegate) {
                    self.undelegate(variable, delegator.clone(), delegee.clone(), right);
                }
            }
            return;
        }
        let accessor = (variable, delegee, right);
        let state = self.delegation_state(&accessor, &delegator);
        match state {
            // Nothing to do here.
            DelegationState::Removed |
            DelegationState::Nonexistant => {}
            DelegationState::Added => self.delegations_added.remove_list(&accessor, &delegator),
            DelegationState::Exists => self.delegations_removed.push(accessor, delegator),
        }
        match state {
            DelegationState::Added | DelegationState::Exists => {
                self.rights_cache.borrow_mut().clear()
            }
            _ => {}
        }
    }

    pub fn set_default_delegator(&mut self, delegator: Symbol) -> Result<(), ExecutionError> {
        if self.principal_exists(delegator) {
            if self.principal == ADMIN {
                self.default_delegator_set = Some(delegator);
                Ok(())
            } else {
                Err(ExecutionError::SecurityViolation)
            }
        } else {
            Err(ExecutionError::PrincipalNotFound)
        }
    }

    pub fn delete_delegation(&mut self,
                             variable: Symbol,
                             delegator: Symbol,
                             right: Right,
                             delegee: Symbol)
                             -> Result<(), ExecutionError> {
        if !self.principal_exists(delegator) || !self.principal_exists(delegee) {
            return Err(ExecutionError::PrincipalNotFound);
        }
        if variable != ALL {
            if let VariableState::NotFound = self.variable_state(variable, None) {
                return Err(ExecutionError::VariableNotFound);
            }
        }
        if self.principal != ADMIN &&
           (delegator != self.principal || !(self.has_right(delegator, variable, Right::Delegate) || variable == ALL)) &&
           self.principal != delegee {
            Err(ExecutionError::SecurityViolation)
        } else {
            self.undelegate(variable, delegator, delegee, right);
            Ok(())
        }
    }

    pub fn set_delegation(&mut self,
                          variable: Symbol,
                          delegator: Symbol,
                          right: Right,
                          delegee: Symbol)
                          -> Result<(), ExecutionError> {
        if !self.principal_exists(delegator) || !self.principal_exists(delegee) {
            return Err(ExecutionError::PrincipalNotFound);
        }
        if variable != ALL {
            if let VariableState::NotFound = self.variable_state(variable, None) {
                return Err(ExecutionError::VariableNotFound);
            }
        }
        if self.principal != ADMIN &&
           (delegator != self.principal || !(self.has_right(delegator, variable, Right::Delegate) || variable == ALL)) {
            Err(ExecutionError::SecurityViolation)
        } else {
            self.delegate(variable, delegator, delegee, right);
            Ok(())
        }
    }

    pub fn create_principal(&mut self,
                            principal: Symbol,
                            password: String)
                            -> Result<(), ExecutionError> {
        if self.principal_exists(principal) {
            return Err(ExecutionError::PrincipalAlreadyExists);
        }
        if self.principal != ADMIN {
            return Err(ExecutionError::SecurityViolation);
        }
        self.principal_modified.insert(principal.clone(), password);
        // Add default delegations
        let delegator = self.default_delegator().to_owned();
        for right in &[Right::Read, Right::Write, Right::Append, Right::Delegate] {
            self.delegate(ALL, delegator.clone(), principal.clone(), *right);
        }
        Ok(())
    }

    pub fn change_password(&mut self,
                           principal: Symbol,
                           password: String)
                           -> Result<(), ExecutionError> {
        if !self.principal_exists(principal) {
            Err(ExecutionError::PrincipalNotFound)
        } else if self.principal != ADMIN && principal != self.principal {
            Err(ExecutionError::SecurityViolation)
        } else {
            self.principal_modified.insert(principal, password);
            Ok(())
        }
    }

    fn delegators(&self, principal: Symbol, variable: Symbol, right: Right) -> Vec<Symbol> {
        let accessor = (variable, principal, right);
        let removed = self.delegations_removed.get(&accessor);
        self.state
            .delegations
            .get(&accessor)
            .iter()
            .flat_map(|i| *i)
            .map(|i| *i)
            .filter(move |d| if let Some(rem) = removed {
                !rem.contains(d)
            } else {
                true
            })
            .chain(self.delegations_added
                .get(&accessor)
                .iter()
                .flat_map(|i| *i)
                .map(|i| *i))
            .collect::<Vec<_>>()
    }

    pub fn require_right(&self,
                         variable: Symbol,
                         right: Right,
                         local_scope: &HashMap<Symbol, Value>)
                         -> Result<(), ExecutionError> {
        if local_scope.contains_key(&variable) || self.has_right(self.principal, variable, right) {
            Ok(())
        } else {
            Err(ExecutionError::SecurityViolation)
        }
    }

    pub fn has_right(&self, principal: Symbol, variable: Symbol, right: Right) -> bool {
        let accessor = (principal, variable, right);
        if principal == ADMIN || self.rights_cache.borrow().contains(&accessor) {
            return true;
        }
        if let VariableState::NotFound = self.variable_state(variable, None) {
            return true;
        }
        if let VariableState::NotFound = self.variable_state(variable, None) {
            return true;
        }
        let mut delegators = HashSet::new();
        delegators.insert(principal);
        delegators.insert(ANYONE);
        let mut unchecked_delegators = vec![ANYONE, principal];
        unchecked_delegators.dedup();
        while let Some(delegator) = unchecked_delegators.pop() {
            if delegator == ADMIN {
                self.rights_cache.borrow_mut().insert(accessor);
                return true;
            }
            for indirect_delegator in self.delegators(delegator, variable, right) {
                if !delegators.contains(&indirect_delegator) {
                    delegators.insert(indirect_delegator);
                    unchecked_delegators.push(indirect_delegator);
                }
            }
        }
        false
    }

    pub fn append<E: Eval<Return=Value>>(&mut self,
                  variable: Symbol,
                  local_scope: &mut HashMap<Symbol, Value>,
                  statuses: &mut Vec<Status>,
                  val: &E)
                  -> Result<(), ExecutionError> {
        let state = self.variable_state(variable, Some(local_scope));
        match state {
            VariableState::Local => {
                if let Some(&Value::List(_)) = local_scope.get(&variable) {
                } else {
                    return Err(ExecutionError::NotAList);
                }
                let val = try!(val.eval(self, statuses, local_scope));
                local_scope.append(&variable, val)
            }
            VariableState::Set => {
                if let Some(&Value::List(_)) = self.value_set.get(&variable) {
                } else {
                    return Err(ExecutionError::NotAList);
                }
                let val = try!(val.eval(self, statuses, local_scope));
                self.value_set.append(&variable, val)
            }
            VariableState::Unchanged => {
                if let Some(&Value::List(_)) = self.state.globals.get(&variable) {
                    let val = try!(val.eval(self, statuses, local_scope));
                    self.value_appended.append(variable, val);
                    Ok(())
                } else {
                    Err(ExecutionError::NotAList)
                }
            }
            VariableState::Appended => {
                let val = try!(val.eval(self, statuses, local_scope));
                self.value_appended.append(variable, val);
                Ok(())
            }
            VariableState::NotFound => Err(ExecutionError::VariableNotFound),
        }
    }

    pub fn set_local<E: Eval<Return=Value>>(&mut self,
                     variable: Symbol,
                     local_scope: &mut HashMap<Symbol, Value>,
                     statuses: &mut Vec<Status>,
                     val: &E)
                     -> Result<(), ExecutionError> {
        let state = self.variable_state(variable, Some(local_scope));
        match state {
            VariableState::Set | VariableState::Appended | VariableState::Unchanged |
            VariableState::Local => Err(ExecutionError::WouldShadow),
            VariableState::NotFound => {
                let val = try!(val.eval(self, statuses, local_scope));
                local_scope.insert(variable, val);
                Ok(())
            }
        }
    }

    pub fn set(&mut self,
               variable: Symbol,
               local_scope: &mut HashMap<Symbol, Value>,
               val: Value)
               -> Result<(), ExecutionError> {
        let state = self.variable_state(variable, Some(local_scope));
        match state {
            VariableState::Local => {
                local_scope.insert(variable, val);
            }
            VariableState::Set | VariableState::Appended | VariableState::Unchanged => {
                // Make sure that the transaction doesn't erroneously append stuff to the new
                // variable
                self.value_appended.remove(&variable);
                self.value_set.insert(variable, val);
            }
            VariableState::NotFound => {
                // We get to create a new variable! Wee!
                self.value_set.insert(variable.clone(), val);
                if self.principal != ADMIN {
                    let principal = self.principal.clone();
                    for right in &[Right::Read, Right::Write, Right::Append, Right::Delegate] {
                        self.delegate(variable.clone(), ADMIN, principal.clone(), *right);
                    }
                }
            }
        }
        Ok(())
    }

    pub fn get<'b>(&'b self,
                   variable: Symbol,
                   local_scope: &'b HashMap<Symbol, Value>)
                   -> Result<Cow<'b, Value>, ExecutionError> {
        let state = self.variable_state(variable, Some(local_scope));
        // Unwraps are safe due to variable state.
        let res = try!(match state {
            VariableState::Local => Ok(Cow::Borrowed(local_scope.get(&variable).unwrap())),
            VariableState::NotFound => Err(ExecutionError::VariableNotFound),
            VariableState::Set => Ok(Cow::Borrowed(self.value_set.get(&variable).unwrap())),
            VariableState::Unchanged => {
                Ok(Cow::Borrowed(self.state.globals.get(&variable).unwrap()))
            }
            VariableState::Appended => {
                let mut res = self.state.globals.get(&variable).unwrap().clone().as_list();
                res.append(&mut self.value_appended.get(&variable).unwrap().clone());
                Ok(Cow::Owned(Value::List(res)))
            }
        });
        Ok(res)
    }

    pub fn apply(mut self) {
        // Add delegations
        for (delegation, delegators) in self.delegations_added {
            if self.state.delegations.contains_key(&delegation) {
                // Unwrap is safe due to contains check.
                self.state.delegations.get_mut(&delegation).unwrap().extend(delegators);
            } else {
                self.state.delegations.insert(delegation, delegators);
            }
        }
        // Remove delegations
        for (delegation, delegators) in self.delegations_removed {
            // Unwrap is safe, as a remove will never get added to a transaction unless it is in
            // the state.
            let list = self.state.delegations.get_mut(&delegation).unwrap();
            // Note: This is quadratic. That's not particularly good, but fortunately is is
            // quadratic with respect to a very specific variable (the number of delegators
            // delegating a specific right to a specific delegee), and shouldn't be a problem
            // expect in esoteric use cases.
            list.retain(|item| !delegators.contains(item));
        }
        // Set the default delegator
        if let Some(default_delegator) = self.default_delegator_set {
            self.state.default_delegator = default_delegator;
        }
        // Modify principals
        for (principal, password) in self.principal_modified {
            self.state.principals.insert(principal, password);
        }
        // Set new values
        for (variable, value) in self.value_set {
            self.state.globals.insert(variable, value);
        }
        // Append to existing values
        for (variable, values) in self.value_appended {
            match self.state.globals.get_mut(&variable) {
                Some(&mut Value::List(ref mut list)) => list.extend(values.into_iter()),
                _ => panic!("Attempted internal append on non-list!"),
            }
        }
    }
}
