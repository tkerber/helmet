# Helmet

*nothing protects against a five dollar wrench... except maybe a helmet.*

Helmet is my submission to the 2016 fall [Build it Break it Fix
it](https://builditbreakit.org) contest.

# Copying

The code and all parts of it are licensed under the MIT license. Due to the
contests restrictions on internet, a (stripped) snapshot of the cargo directory
is kept in `build/cargo.tar.gz`. This snapshot contains the source var several
libraries used, all of which also fall under the MIT and/or apache licenses.
The licenses are preserved in the relevant directories of the tar file.
