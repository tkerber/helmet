#[macro_use]
extern crate nom;
#[macro_use]
extern crate chan;
extern crate chan_signal;

mod parse;
mod state;
mod eval;
mod srv;
mod symbol;

use std::env::args;
use std::process::exit;
use std::io::ErrorKind;
use std::net::Ipv4Addr;
use srv::srv;
use chan_signal::Signal;
use std::thread;

// May seem big, but virtual memory is bigger!
const STACK_SIZE: usize = 1 << 30;

fn parse_port(from: &str) -> u16 {
    if from.len() > 4096 {
        exit(255);
    }
    let port = match from.parse() {
        Ok(v) => v,
        Err(_) => exit(255),
    };
    // Since rust allows preceding zeros, discard them manually.
    // Note that '0' is not a valid port anyway, so it doesn't matter that this is also discarded.
    if from.as_bytes()[0] == '0' as u8 {
        exit(255);
    }
    if port < 1024 {
        exit(255);
    }
    port
}

fn parse_admin_password(from: &str) -> String {
    if from.len() > 4096 {
        exit(255);
    }
    let mut cp = from.as_bytes().to_owned();
    cp.sort();
    cp.dedup();
    if cp.iter().all(|c| parse::STRING.contains(c)) {
        from.to_owned()
    } else {
        exit(255)
    }
}

fn run(_sdone: chan::Sender<()>, port: u16, admin_password: Option<String>) {
    if let Err(e) = srv((Ipv4Addr::new(127, 0, 0, 1), port), admin_password) {
        if e.kind() == ErrorKind::AddrInUse {
            exit(63);
        } else {
            // Not specified, but the specification makes no provisions for unexpected OS errors.
            exit(1);
        }
    }
}

fn main() {
    let args = args().collect::<Vec<_>>();
    if args.len() != 2 && args.len() != 3 {
        exit(255);
    }
    let port = parse_port(&args[1]);
    let admin_password = if args.len() == 3 {
        Some(parse_admin_password(&args[2]))
    } else {
        None
    };

    let signal = chan_signal::notify(&[Signal::INT, Signal::TERM]);
    let (sdone, rdone) = chan::sync(0);
    if let Err(_) = thread::Builder::new()
        .stack_size(STACK_SIZE)
        .spawn(move || run(sdone, port, admin_password)) {
        exit(1);
    }

    chan_select! {
        signal.recv() => {},
        rdone.recv() => {},
    }
}
