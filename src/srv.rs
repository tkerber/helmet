use std::net::{ToSocketAddrs, TcpListener, TcpStream};
use std::io::{self, Read, BufWriter, Write};
use std::time::{Instant, Duration};
use eval::{Status, eval};
use state::State;

pub fn srv<A: ToSocketAddrs>(addr: A, admin_pass: Option<String>) -> io::Result<()> {
    let mut state = State::new(admin_pass);
    let socket = try!(TcpListener::bind(addr));
    loop {
        let (stream, _) = try!(socket.accept());
        // Discard any errors that occur in a stream. Errors here are no reason to crash.
        if let Ok(true) = serve_stream(stream, &mut state) {
            break;
        }
    }
    Ok(())
}

enum RecvState<V> {
    Ok(V),
    TooLong,
    Eof,
    Timeout,
}

fn serve_stream(mut stream: TcpStream, state: &mut State) -> io::Result<bool> {
    let data = {
        let mut buf = Vec::new();
        let mut res = None;
        const BUF_SIZE: usize = 1 << 14;
        fn find_seq(buf: &[u8], seq: &[u8]) -> Option<usize> {
            for i in 0..buf.len() {
                if buf[i..].starts_with(seq) {
                    return Some(i);
                }
            }
            None
        }
        let t0 = Instant::now();
        let timeout = Duration::from_secs(30);
        let mut found_seq = false;
        loop {
            let elapsed_time = Instant::now() - t0;
            if timeout < elapsed_time {
                res = Some(RecvState::Timeout);
                break;
            }
            let read_timeout = if find_seq(&buf[..], b"***").is_some() {
                found_seq = true;
                // Make sure to read all the small stuff. If nothing is left, the read will timeout
                // (almost) immediately.
                Duration::from_millis(5)
            } else {
                timeout - elapsed_time
            };
            let l = buf.len();
            buf.resize(l + BUF_SIZE, 0u8);
            try!(stream.set_read_timeout(Some(read_timeout)));
            let read = match stream.read(&mut buf[l..]) {
                Ok(v) => v,
                Err(ref e) if e.kind() == io::ErrorKind::TimedOut ||
                              e.kind() == io::ErrorKind::WouldBlock => {
                    buf.truncate(l);
                    if !found_seq {
                        res = Some(RecvState::Timeout);
                    }
                    break;
                }
                Err(ref e) if e.kind() == io::ErrorKind::UnexpectedEof => {
                    buf.truncate(l);
                    if !found_seq {
                        res = Some(RecvState::Eof);
                    }
                    break;
                }
                Err(e) => return Err(e),
            };
            buf.truncate(l + read);
        }
        // Discard what isn't part of the program.
        res.unwrap_or_else(move || {
            // Safe as res == None iff the while loop finished naturally => find_seq(&buf[..],
            // b"***") != None
            let l = find_seq(&buf[..], b"***").unwrap() + 3;
            buf.truncate(l);
            if l > 1000000 {
                RecvState::TooLong
            } else {
                RecvState::Ok(buf)
            }
        })
    };
    let mut writer = BufWriter::new(&mut stream);
    match data {
        RecvState::Ok(prog) => {
            let mut exit = false;
            let (statuses, transaction) = eval(prog.as_ref(), state);
            for status in statuses {
                try!(writeln!(writer, "{}", status));
                exit = if let Status::Exiting = status {
                    true
                } else {
                    false
                };
            }
            try!(writer.flush());
            if let Some(t) = transaction {
                t.apply();
            }
            Ok(exit)
        }
        RecvState::Timeout => {
            try!(writeln!(writer, "{}", Status::Timeout));
            Ok(false)
        }
        RecvState::Eof | RecvState::TooLong => {
            try!(writeln!(writer, "{}", Status::Failed));
            Ok(false)
        }
    }
}
