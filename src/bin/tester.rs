//! A rough and shoddy client program which connects to a server, runs a file, and prints the
//! response to stdout.

extern crate json;

use std::env::{current_exe, args};
use std::net::{Ipv4Addr, TcpStream};
use std::fs::File;
use std::io::{stdout, stderr, Read, Write};
use std::process::{Command, exit};
use std::thread::sleep;
use std::time::Duration;

fn main() {
    let args = args().collect::<Vec<_>>();
    let mut server = current_exe().unwrap();
    server.pop();
    server.push("server");
    let mut buf = Vec::new();
    File::open(&args[1]).unwrap().read_to_end(&mut buf).unwrap();
    let json = json::parse(String::from_utf8_lossy(buf.as_ref()).as_ref()).unwrap();
    let args = if json["arguments"].is_object() {
            &json["arguments"]["argv"]
        } else {
            &json["arguments"]
        }
        .members()
        .map(|v| v.as_str().unwrap())
        .map(|s| if s == "%PORT%" { &args[2] } else { s })
        .collect::<Vec<_>>();
    let mut srv = Command::new(server)
        .args(&args)
        .spawn()
        .unwrap();
    sleep(Duration::from_millis(100));
    let port = args[0];
    let mut passed = true;
    for prog in json["programs"].members() {
        let input = if prog.is_object() {
            &prog["program"]
        } else {
            prog
        };
        let mut stream = TcpStream::connect((Ipv4Addr::new(127, 0, 0, 1), port.parse().unwrap()))
            .unwrap();
        stream.write_all(input.as_str().unwrap().as_ref()).unwrap();
        let mut buf = Vec::new();
        sleep(Duration::from_millis(100));
        stream.read_to_end(&mut buf).unwrap();
        if prog.is_object() {
            let borrow = String::from_utf8_lossy(&buf[..]);
            let mut lines = borrow.split('\n');
            let mut members = prog["output"].members();
            loop {
                let (line, member) = (lines.next(), members.next());
                match (line, member) {
                    (Some(line), Some(member)) => {
                        let json = json::parse(line).unwrap();
                        if &json != member {
                            writeln!(stderr(), "Expected '{}', got '{}'", member, json).unwrap();
                            passed = false;
                            break;
                        }
                    }
                    (Some(line), None) => {
                        if line.len() > 0 {
                            writeln!(stderr(), "Expected end, got '{}'", line).unwrap();
                            passed = false;
                            break;
                        }
                    }
                    (None, Some(member)) => {
                        writeln!(stderr(), "Expected '{}', got nothing", member).unwrap();
                        passed = false;
                        break;
                    }
                    (None, None) => {
                        break;
                    }
                }
            }
        } else {
            stdout().write_all(&buf[..]).unwrap();
        }
    }
    srv.kill().unwrap();
    if passed {
        exit(0);
    } else {
        exit(1)
    }
}
