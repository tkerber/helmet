//! A rough and shoddy client program which connects to a server, runs a file, and prints the
//! response to stdout.

use std::env::args;
use std::fs::File;
use std::io::{stdout, Read, Write};
use std::net::TcpStream;

fn main() {
    // Arguments:
    //
    // 1. server URL (e.g. localhost:3000)
    // 2. Program file name
    let args = args().collect::<Vec<_>>();
    let mut buf = Vec::new();
    File::open(&args[2]).unwrap().read_to_end(&mut buf).unwrap();
    let mut stream = TcpStream::connect(&*args[1]).unwrap();
    stream.write_all(&buf[..]).unwrap();
    buf.clear();
    stream.read_to_end(&mut buf).unwrap();
    stdout().write_all(&buf[..]).unwrap();
}
