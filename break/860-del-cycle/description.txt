This test case puts the server into an infinite loop, presenting an attack on availability.
