This test case crashes the program when it attempts to resolve a delegation cycle. As the program terminates without recieving an exit command or OS signal, this violates the spec.
