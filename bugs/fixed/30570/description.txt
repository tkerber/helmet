Not sure why this team does not like 'local' statements prefixed by spaces. Per spec, such a statement must be allowed and accepted.

Expected output: [{u'status': u'DENIED'}]
Real output: [{u'status': u'FAILED'}]
