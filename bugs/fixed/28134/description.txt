The second program should result in a security violation because bob does not have permission to write x.
The output of this program should be {"status":"DENIED"}

Team 898
Output does not match expected output. Returns {"status":"FAILED"}