#!/usr/bin/python
import json

breaks = [
    '26143', '26352', '26444', '27193', '27219',
    '27607', '28071', '28339', '28754', '30552',
    '30560', '30601', '31097', '31407', '31593',
    '31668', '31693', '31695', '31710', '26331',
    '26360', '26867', '27202', '27225', '27637',
    '28134', '28469', '30551', '30556', '30570',
    '30959', '31308', '31432', '31605', '31692',
    '31694', '31696', '31736']

for br in breaks:
    f = open(br + '/query.json')
    j1 = json.load(f)
    f.close()
    f = open(br + '/resp.json')
    j2 = json.load(f)
    f.close()
    j3 = {
        'arguments': {'argv': j1['arguments']},
        'programs': [{'program': p, 'output': o} for (p, o) in zip(j1['programs'], j2['output'])],
        'return_code': j2['return_code'],
    }
    f = open(br + '/oracletest.json', 'w')
    json.dump(j3, f)
    f.close()
