#!/bin/bash
cargo build --release
for f in build/tests/*.json bugs/fixed/*/oracletest.json bugs/*/oracletest.json
do
    cargo run --release --bin tester "$f" "$RANDOM" > /dev/null 2>&1
    if [ $? -eq 0 ]
    then
        echo "test passed: $f"
    else
        echo "test failed: $f"
    fi
done
